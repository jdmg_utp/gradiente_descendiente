
x = load('./ex2Data/ex2x.dat');
y = load('./ex2Data/ex2y.dat');
X=[];
W=[0 rand()];
m= length(y); ;
X = [ones(m, 1) x]; 
MAX_ITR = 500;
alpha = 0.07;
W = zeros(size(X(1,:)))';
J = [];
for num_iterations = 1:MAX_ITR
	for m_iter=1:m
		sum_trans=(W(1)*X(m_iter,1)+W(2)*X(m_iter,2))-y(m_iter);
		w0cpy=W(1)-alpha*(1/m)*sum_trans*X(m_iter,1);
		w1cpy=W(2)-alpha*(1/m)*sum_trans*X(m_iter,2);
		W(1)=w0cpy;
		W(2)=w1cpy;
	end
	temp=0.0;
	for m_iter=1:m
		temp=temp+((W(1)*X(m_iter,1)+W(2)*X(m_iter,2))-y(m_iter))^2;
	end
	J=[J (1/(2*m))*temp];	

end

disp("Graficando datos")
plot(x, y, 'o');
disp("presione una tecla para continuar")
pause;
hold on
disp("Graficando recta")
plot(X(:,2),(W(1)*X(:,1)+W(2)*X(:,2)),'r');
pause;
hold off 

plot(J,'r')
ylabel('J(W)')

